# @zwerg/core

> This package provides basic types to applications

![Pipeline status](https://gitlab.com/zwerg1/core/badges/master/pipeline.svg)
![Test Coverage](https://gitlab.com/zwerg1/core/badges/master/coverage.svg)

## Install

```bash
npm i -S @zwerg/core
```

## Usage

#### Functional interfaces
```typescript
// Function without parameters returning nothing
const action: Action = () => {};

// Function which just returns a value
const supplier: Supplier<string> = () => "Hello";

// Function which accepts one parameter
const consumer: Consumer<string> = (str: string) => {};

// Function which accepts two parameters
const biConsumer: BiConsumer<string, number> = (str: string, num: number) => {};

// Function which accepts one parameter and returns a value
const uniFunction: UniFunction<string, number> = (str: string) => str.length;

// Function which accepts two parameters and returns a value
const biFunction: BiFunction<number, number, number> = (a: number, b: number) => a * b;

// Function which accepts three parameters and returns a value
const triFunction: TriFunction<number, number, number, number> = (a: number, b: number, c: number) => a * b * c;
```

#### Type interfaces
```typescript
// Store any type as a variable (like 'Class' in Java)
const myType: Type<string> = String;
```

#### Business Interfaces
```typescript
// Conversion Interface
class StringToLengthConverter implements Converter<string, number> {
    public convert(item: string): number {
        return item.length;
    }
}

// Validation Interface
enum ErrorType {
    IS_EMPTY
}

class CustomStringValidator implements Validator<string, ErrorType> {
    private readonly pattern = /^\s+$/;

    public validate(object: string): Array<ErrorType> {
        if (this.pattern.test(object)) {
            return [ErrorType.IS_EMPTY];
        } else {
            return [];
        }
    }
}
```

## License

> MIT License
> 
> Copyright (c) 2020 Tobias Glatthar
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
> 
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.