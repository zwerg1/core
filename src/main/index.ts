export {Action, Supplier, Consumer, BiConsumer, UniFunction, BiFunction, TriFunction} from "./basic/functional-interfaces";
export {Type} from "./basic/types";

export {Converter} from "./app/converter";
export {Validator} from "./app/validator";
