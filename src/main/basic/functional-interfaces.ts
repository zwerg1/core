export type Action = () => void;

export type Supplier<TResult> = () => TResult;

export type Consumer<TParam> = (p: TParam) => void;
export type BiConsumer<TParam1, TParam2> = (p1: TParam1, p2: TParam2) => void;

export type UniFunction<TParam, TResult> = (p: TParam) => TResult;
export type BiFunction<TParam1, TParam2, TResult> = (p1: TParam1, p2: TParam2) => TResult;
export type TriFunction<TParam1, TParam2, TParam3, TResult> = (p1: TParam1, p2: TParam2, p3: TParam3) => TResult;
