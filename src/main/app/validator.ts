export interface Validator<T, TValidationError> {
    validate(object: T): Array<TValidationError>;
}
