export interface Converter<T, R> {
    convert(item: T): R;
}
